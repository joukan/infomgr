#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "infomgr.h"

int r_cb(struct infomgr_report *ir)
{
	fprintf(stderr, "=== Report ===\n");
	fprintf(stderr,
		"CPU: %4.1f us, %4.1f sy, %4.1f ni, %4.1f id, %4.1f io, %4.1f irq, %4.1f sirq\n",
		ir->cpu.usr * 100, ir->cpu.sys * 100, ir->cpu.nic * 100,
		ir->cpu.idle * 100, ir->cpu.io * 100, ir->cpu.irq * 100,
		ir->cpu.sirq * 100);
	return 0;
}

int raw_cb(struct infomgr_raw_report *ir)
{
	fprintf(stderr, "=== Raw Report ===\n");
	fprintf(stderr, "CPU: %lldid %lldio\n", (long long)ir->cpu.idle,
		(long long)ir->cpu.iowait);
	return 0;
}

int main(int argc, char *argv[])
{
	int ret = 0;

	fprintf(stderr, "%s-%d\n", __func__, __LINE__);

	ret = infomgr_rcv("test@test", r_cb, raw_cb);

	while (1) {
		sleep(10);
	}

	return ret;
}
