#![allow(unused)]
#![allow(clippy::all)]

include!(concat!(env!("OUT_DIR"), "/infomgr_generated.rs"));

use {
    infomgr_fb::report_fb::{InfomgrRawReport, InfomgrReport},
    std::fmt,
};

impl fmt::Display for InfomgrReport {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\n{}", self.cpu(), self.memory())
    }
}

impl fmt::Display for InfomgrRawReport {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\n{}", self.cpu(), self.memory())
    }
}
