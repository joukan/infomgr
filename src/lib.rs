pub mod flat_buffer;
pub mod infomgr_con_c;
pub mod util;

#[allow(unused)]
use {
    error_stack::{Report, Result, ResultExt},
    flat_buffer::infomgr_fb::report_fb::{
        root_as_data_set, DataSet, InfomgrRawReport, InfomgrReport,
    },
    ipcon_sys::{
        ipcon::{
            Ipcon, IPCON_KERNEL_GROUP_NAME, IPCON_KERNEL_NAME, IPF_DISABLE_KEVENT_FILTER,
            IPF_RCV_IF, IPF_SND_IF,
        },
        ipcon_error::IpconError,
        ipcon_msg::{IpconMsg, IpconMsgType},
    },
    jlogger_tracing::{jdebug, jerror, jinfo, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat},
    std::{
        sync::{
            atomic::{AtomicBool, Ordering},
            mpsc::{Receiver, Sender},
            Arc,
        },
        thread::{self, JoinHandle},
    },
    util::InfomgrError,
};

pub enum ReportData {
    Report(InfomgrReport),
    RawReport(InfomgrRawReport),
}

pub struct InfomgrReceiver {
    should_exit: Arc<AtomicBool>,
    handler: Option<JoinHandle<Result<(), InfomgrError>>>,
}

impl InfomgrReceiver {
    pub fn start(
        source: &str,
        sender: Sender<ReportData>,
        timeout: Option<u32>,
    ) -> Result<Self, InfomgrError> {
        let should_exit = Arc::new(AtomicBool::new(false));
        let should_exit_ = should_exit.clone();
        let source = source.to_string();

        let handler = thread::spawn(move || -> Result<(), InfomgrError> {
            let (group, peer) = source
                .split_once('@')
                .ok_or(Report::new(InfomgrError::InvalidData))?;

            let ih = Ipcon::new(None, Some(IPF_RCV_IF | IPF_DISABLE_KEVENT_FILTER))
                .change_context(InfomgrError::IpconError)?;

            ih.join_group(IPCON_KERNEL_NAME, IPCON_KERNEL_GROUP_NAME)
                .change_context(InfomgrError::IpconError)?;

            let _ = ih.join_group(peer, group);
            let matched = |p: &str, g: &str| -> bool { p == peer && g == group };
            let timeout = timeout.unwrap_or(3);

            loop {
                if should_exit_.load(Ordering::Acquire) {
                    return Ok(());
                }

                match ih.receive_msg_timeout(timeout, 0) {
                    Err(e) => {
                        if matches!(e.current_context(), IpconError::SysErrorTimeOut) {
                            continue;
                        } else {
                            return Err(Report::new(InfomgrError::IpconError));
                        }
                    }
                    Ok(msg) => match msg {
                        IpconMsg::IpconMsgKevent(kevent) => {
                            if let Some((p, g)) = kevent.group_added() {
                                if matched(&p, &g) && ih.join_group(&p, &g).is_ok() {
                                    jinfo!("Infomgr: {}@{} connected.", g, p);
                                }
                            }

                            if let Some((p, g)) = kevent.group_removed() {
                                jinfo!("Infomgr: {}@{} lost.", g, p);
                            }
                        }

                        IpconMsg::IpconMsgUser(m)
                            if matches!(m.msg_type, IpconMsgType::IpconMsgTypeGroup) =>
                        {
                            let group = m.group.unwrap();
                            if matched(&m.peer, &group) {
                                let ds = root_as_data_set(&m.buf)
                                    .map_err(|_| InfomgrError::InvalidData)?;

                                if let Some(r) = ds.info_report() {
                                    let data =
                                        ReportData::Report(InfomgrReport::new(r.cpu(), r.memory()));
                                    sender
                                        .send(data)
                                        .map_err(|_| Report::new(InfomgrError::IOError))?;
                                }

                                if let Some(r) = ds.info_raw_report() {
                                    let data = ReportData::RawReport(InfomgrRawReport::new(
                                        r.cpu(),
                                        r.memory(),
                                    ));
                                    sender
                                        .send(data)
                                        .map_err(|_| Report::new(InfomgrError::IOError))?;
                                }
                            }
                        }

                        IpconMsg::IpconMsgUser(m) => {
                            jwarn!("Suspicious message from {}", m.peer);
                        }

                        IpconMsg::IpconMsgInvalid => {
                            return Err(Report::new(InfomgrError::IpconError))
                        }
                    },
                }
            }
        });

        Ok(InfomgrReceiver {
            should_exit,
            handler: Some(handler),
        })
    }

    pub fn stop(&mut self) -> Result<(), InfomgrError> {
        self.should_exit.store(true, Ordering::Release);
        if let Some(handler) = self.handler.take() {
            handler
                .join()
                .map_err(|_| Report::new(InfomgrError::Unexpected))??;
        }
        Ok(())
    }
}
