#[allow(unused)]
use {
    clap::Parser,
    error_stack::{Report, Result, ResultExt},
    infomgr_con::{
        flat_buffer::infomgr_fb::report_fb::{
            CpuReport, CpuTime, DataSet, DataSetArgs, InfomgrRawReport, InfomgrReport, MemoryReport,
        },
        util::{cpuload::CpuTimeDiff, meminfo::MemInfo, InfomgrError},
    },
    ipcon_sys::ipcon::{
        Ipcon, IPCON_KERNEL_GROUP_NAME, IPCON_KERNEL_NAME, IPF_DISABLE_KEVENT_FILTER, IPF_RCV_IF,
        IPF_SND_IF,
    },
    jlogger_tracing::{jdebug, jerror, jinfo, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat},
    std::sync::{mpsc, Arc},
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Cli {
    /// Name of the Sender.
    /// It should be "<group name>@<peer name)" in case of using IPCON.
    #[clap(short, long, default_value_t=String::from("fake"))]
    name: String,

    /// Sending interval.
    #[clap(short, long, default_value_t = 1)]
    interval: u64,

    /// Sending raw infomgr report.
    #[clap(short, long)]
    raw: bool,

    #[arg(short, long, action=clap::ArgAction::Count)]
    verbose: u8,
}

fn ipcon_init(cli: &Cli) -> Result<(Ipcon, &str), InfomgrError> {
    let (group, peer) = cli
        .name
        .split_once('@')
        .ok_or(Report::new(InfomgrError::InvalidData))?;

    let ih = Ipcon::new(Some(peer), Some(IPF_SND_IF)).change_context(InfomgrError::IpconError)?;
    ih.register_group(group)
        .change_context(InfomgrError::IpconError)?;
    Ok((ih, group))
}

#[allow(unused)]
fn report(cli: &Cli) -> Result<(), InfomgrError> {
    let (ih, group) = ipcon_init(cli)?;
    let interval = cli.interval;

    let mut cpu_start = CpuTime::load().unwrap();
    loop {
        std::thread::sleep(std::time::Duration::from_secs(interval));
        let cpu_end = CpuTime::load().unwrap();

        let mut report: InfomgrReport =
            InfomgrReport::new(&(cpu_end - cpu_start).cpu_report(), &MemInfo::load());

        jdebug!("{}", report);

        let mut builder = flatbuffers::FlatBufferBuilder::with_capacity(1024);
        let ds = DataSet::create(
            &mut builder,
            &DataSetArgs {
                info_report: Some(&report),
                info_raw_report: None,
            },
        );

        builder.finish(ds, None);
        let buf = builder.finished_data();

        ih.send_multicast(group, buf, false)
            .change_context(InfomgrError::IpconError)?;
    }
}

#[allow(unused)]
fn raw_report(cli: &Cli) -> Result<(), InfomgrError> {
    let (ih, group) = ipcon_init(cli)?;
    let interval = cli.interval;

    loop {
        let mut report = InfomgrRawReport::new(&CpuTime::load().unwrap(), &MemInfo::load());

        let mut builder = flatbuffers::FlatBufferBuilder::with_capacity(1024);
        let ds = DataSet::create(
            &mut builder,
            &DataSetArgs {
                info_report: None,
                info_raw_report: Some(&report),
            },
        );

        builder.finish(ds, None);
        let buf = builder.finished_data();

        ih.send_multicast(group, buf, false)
            .change_context(InfomgrError::IpconError)?;

        std::thread::sleep(std::time::Duration::from_secs(interval));
    }
}

fn main() -> Result<(), InfomgrError> {
    let cli = Cli::parse();

    let level = match cli.verbose {
        1 => LevelFilter::DEBUG,
        2 => LevelFilter::TRACE,
        _ => LevelFilter::INFO,
    };

    JloggerBuilder::new()
        .max_level(level)
        .log_time(LogTimeFormat::TimeStamp)
        .build();

    if cli.raw {
        raw_report(&cli)
    } else {
        report(&cli)
    }
}
