pub mod cpuload;
pub mod meminfo;

#[allow(unused)]
use {
    super::flat_buffer::infomgr_fb::report_fb::{InfomgrRawReport, InfomgrReport},
    error_stack::{Report, Result, ResultExt},
    std::{error::Error, fmt},
};

#[derive(Debug)]
pub enum InfomgrError {
    IOError,
    InvalidData,

    #[cfg(feature = "enable_ipcon")]
    IpconError,

    Unexpected,
}

impl fmt::Display for InfomgrError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        let err_str = match self {
            InfomgrError::IOError => "IO error",
            InfomgrError::InvalidData => "Invalid data",

            #[cfg(feature = "enable_ipcon")]
            InfomgrError::IpconError => "Ipcon error",

            _ => "Unexpected data",
        };

        write!(f, "{}", err_str)
    }
}

impl Error for InfomgrError {}
