#[allow(unused)]
use {
    crate::flat_buffer::infomgr_fb::report_fb::MemoryReport,
    jlogger_tracing::{jdebug, jerror, jinfo, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat},
    std::{
        fmt, fs,
        io::{BufRead, BufReader},
    },
};

impl fmt::Display for MemoryReport {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s: Vec<String> = Vec::new();

        s.push(format!("{} total,", self.total()));
        s.push(format!("{} free,", self.free()));
        s.push(format!("{} avail,", self.avail()));
        s.push(format!("{} anon,", self.anon()));
        s.push(format!("{} file,", self.file()));
        s.push(format!("{} buffer,", self.buffer()));
        s.push(format!("{} cached,", self.cached()));
        s.push(format!("{} unevict,", self.unevictable()));
        s.push(format!("{} cma,", self.cmatotal()));
        s.push(format!("{} cmafree,", self.cmafree()));
        s.push(format!("{} slab,", self.slab()));
        s.push(format!("{} slab_unrec,", self.slab_unrec()));
        s.push(format!("{} shmem,", self.shmem()));

        write!(f, "Memory(kb): {}", s.join(" ").trim_end_matches(','))
    }
}

pub struct MemInfo {}

impl MemInfo {
    pub fn load() -> MemoryReport {
        let mut mr: MemoryReport = Default::default();
        mr.set_file(0);
        mr.set_anon(0);

        if let Ok(f) = fs::OpenOptions::new().read(true).open("/proc/meminfo") {
            let mut reader = BufReader::new(f);

            loop {
                let mut line = String::new();

                if reader.read_line(&mut line).is_err() {
                    break;
                }

                if line.trim().is_empty() {
                    break;
                }

                let mut entries = line.split_whitespace();

                let (name, size_kb) = (
                    entries.next().unwrap().trim(),
                    entries.next().unwrap().parse::<u64>().unwrap(),
                );

                match name {
                    "MemTotal:" => mr.set_total(size_kb),
                    "MemFree:" => mr.set_free(size_kb),
                    "Active(anon):" => mr.set_anon(mr.anon() + size_kb),
                    "Inactive(anon):" => mr.set_anon(mr.anon() + size_kb),
                    "Active(file):" => mr.set_file(mr.file() + size_kb),
                    "Inactive(file):" => mr.set_file(mr.file() + size_kb),
                    "MemAvailable:" => mr.set_avail(size_kb),
                    "Buffers:" => mr.set_buffer(size_kb),
                    "Cached:" => mr.set_cached(size_kb),
                    "Unevictable:" => mr.set_unevictable(size_kb),
                    "CmaTotal:" => mr.set_cmatotal(size_kb),
                    "CmaFree:" => mr.set_cmafree(size_kb),
                    "Slab:" => mr.set_slab(size_kb),
                    "SReclaimable:" => mr.set_slab_rec(size_kb),
                    "SUnreclaim:" => mr.set_slab_unrec(size_kb),
                    "Shmem:" => mr.set_shmem(size_kb),
                    _ => {}
                }
            }
        }
        mr
    }
}
