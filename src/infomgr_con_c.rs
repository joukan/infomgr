#[allow(unused)]
use {
    super::{
        flat_buffer::infomgr_fb::report_fb::{
            root_as_data_set, CpuReport, CpuTime, DataSet, InfomgrRawReport, InfomgrReport,
            MemoryReport,
        },
        util::InfomgrError,
        InfomgrReceiver, ReportData,
    },
    error_stack::{Report, Result, ResultExt},
    ipcon_sys::{
        ipcon::{
            Ipcon, IPCON_KERNEL_GROUP_NAME, IPCON_KERNEL_NAME, IPF_DISABLE_KEVENT_FILTER,
            IPF_RCV_IF, IPF_SND_IF,
        },
        ipcon_msg::{IpconMsg, IpconMsgType},
    },
    jlogger_tracing::{jdebug, jerror, jinfo, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat},
    std::{
        convert,
        ffi::{CStr, CString},
        sync::{
            atomic::{AtomicBool, Ordering},
            mpsc::{self, Receiver, Sender},
            Arc,
        },
        thread::{self, JoinHandle},
    },
};

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct CCpuReport {
    usr: f32,
    sys: f32,
    nic: f32,
    idle: f32,
    io: f32,
    irq: f32,
    sirq: f32,
}

impl From<&CpuReport> for CCpuReport {
    fn from(value: &CpuReport) -> Self {
        CCpuReport {
            usr: value.usr(),
            sys: value.sys(),
            nic: value.nic(),
            idle: value.idle(),
            io: value.io(),
            irq: value.irq(),
            sirq: value.sirq(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct CCpuTime {
    usr: i64,
    nice: i64,
    system: i64,
    idle: i64,
    iowait: i64,
    irq: i64,
    softirq: i64,
}

impl From<&CpuTime> for CCpuTime {
    fn from(value: &CpuTime) -> Self {
        CCpuTime {
            usr: value.usr(),
            nice: value.nice(),
            system: value.system(),
            idle: value.idle(),
            iowait: value.iowait(),
            irq: value.irq(),
            softirq: value.softirq(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct CMemoryReport {
    total: u64,
    free: u64,
    anon: u64,
    file: u64,
    avail: u64,
    buffer: u64,
    cached: u64,
    unevictable: u64,
    cmatotal: u64,
    cmafree: u64,
    slab: u64,
    slab_rec: u64,
    slab_unrec: u64,
    shmem: u64,
}

impl From<&MemoryReport> for CMemoryReport {
    fn from(value: &MemoryReport) -> Self {
        CMemoryReport {
            total: value.total(),
            free: value.free(),
            anon: value.anon(),
            file: value.file(),
            avail: value.avail(),
            buffer: value.buffer(),
            cached: value.cached(),
            unevictable: value.unevictable(),
            cmatotal: value.cmatotal(),
            cmafree: value.cmafree(),
            slab: value.slab(),
            slab_rec: value.slab_rec(),
            slab_unrec: value.slab_unrec(),
            shmem: value.shmem(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct CInfomgrReport {
    cpu: CCpuReport,
    memory: CMemoryReport,
}

impl From<&InfomgrReport> for CInfomgrReport {
    fn from(value: &InfomgrReport) -> Self {
        CInfomgrReport {
            cpu: CCpuReport::from(value.cpu()),
            memory: CMemoryReport::from(value.memory()),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
struct CInfomgrRawReport {
    cpu: CCpuTime,
    memory: CMemoryReport,
}

impl From<&InfomgrRawReport> for CInfomgrRawReport {
    fn from(value: &InfomgrRawReport) -> Self {
        CInfomgrRawReport {
            cpu: CCpuTime::from(value.cpu()),
            memory: CMemoryReport::from(value.memory()),
        }
    }
}

type NotifyReport = extern "C" fn(r: *const CInfomgrReport) -> i32;
type NotifyRawReport = extern "C" fn(r: *const CInfomgrRawReport) -> i32;

#[no_mangle]
extern "C" fn infomgr_rcv(
    source: *const libc::c_char,
    r_cb: Option<NotifyReport>,
    raw_cb: Option<NotifyRawReport>,
) -> i32 {
    if source.is_null() {
        return -22;
    }

    let mut src = String::new();
    unsafe {
        if let Ok(source) = CStr::from_ptr(source).to_str() {
            src = source.to_owned();
        }
    }

    if src.is_empty() {
        return -22;
    }

    let _ = thread::spawn(move || {
        let (s, r) = mpsc::channel();

        if let Ok(mut receiver) = InfomgrReceiver::start(&src, s, Some(1)) {
            while let Ok(msg) = r.recv() {
                match msg {
                    ReportData::Report(r) => {
                        if let Some(cb) = r_cb {
                            let ret = cb(&CInfomgrReport::from(&r));
                            if ret < 0 {
                                break;
                            }
                        }
                    }
                    ReportData::RawReport(r) => {
                        if let Some(cb) = raw_cb {
                            let ret = cb(&CInfomgrRawReport::from(&r));
                            if ret < 0 {
                                break;
                            }
                        }
                    }
                }
            }

            let _ = receiver.stop();
        }
    });

    0
}
