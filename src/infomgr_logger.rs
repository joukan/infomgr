#[allow(unused)]
use {
    clap::Parser,
    error_stack::{Report, Result, ResultExt},
    infomgr_con::{
        flat_buffer::infomgr_fb::report_fb::{root_as_data_set, InfomgrRawReport, InfomgrReport},
        util::{cpuload::CpuTimeDiff, meminfo::MemInfo, InfomgrError},
        InfomgrReceiver, ReportData,
    },
    ipcon_sys::{
        ipcon::{
            Ipcon, IPCON_KERNEL_GROUP_NAME, IPCON_KERNEL_NAME, IPF_DISABLE_KEVENT_FILTER,
            IPF_RCV_IF, IPF_SND_IF,
        },
        ipcon_msg::{IpconMsg, IpconMsgType},
    },
    jlogger_tracing::{jdebug, jerror, jinfo, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat},
    rand::prelude::*,
    std::{fmt, sync::mpsc},
    tracing::{debug, error, info, warn},
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Cli {
    /// Name of the Receiver. Format should be "<group name>@<peer name)" in case of IPCON
    name: String,

    #[arg(short, long, action=clap::ArgAction::Count)]
    verbose: u8,
}

fn main() -> Result<(), InfomgrError> {
    let cli = Cli::parse();

    let level = match cli.verbose {
        1 => LevelFilter::DEBUG,
        2 => LevelFilter::TRACE,
        _ => LevelFilter::INFO,
    };

    JloggerBuilder::new()
        .max_level(level)
        .log_time(LogTimeFormat::TimeStamp)
        .build();

    let (s, r) = mpsc::channel();

    let mut receiver = InfomgrReceiver::start(&cli.name, s, None)?;

    loop {
        match r.recv() {
            Ok(ReportData::Report(r)) => {
                println!("{}", r);
            }
            Ok(ReportData::RawReport(r)) => {
                println!("{}", r);
            }
            Err(_) => {
                break;
            }
        }
    }

    receiver.stop()
}
