#ifndef __INFORMGR_H__
#define __INFORMGR_H__

#include <stdint.h>

struct cpu_report {
	float usr;
	float sys;
	float nic;
	float idle;
	float io;
	float irq;
	float sirq;
};

struct cpu_time {
	int64_t usr;
	int64_t nice;
	int64_t system;
	int64_t idle;
	int64_t iowait;
	int64_t irq;
	int64_t softirq;
};

struct memory_report {
	int64_t total;
	int64_t free;
	int64_t anon;
	int64_t file;
	int64_t avail;
	int64_t buffer;
	int64_t cached;
	int64_t unevictable;
	int64_t cmatotal;
	int64_t cmafree;
	int64_t slab;
	int64_t slab_rec;
	int64_t slab_unrec;
	int64_t shmem;
};

struct infomgr_report {
	struct cpu_report cpu;
	struct memory_report memory;
};

struct infomgr_raw_report {
	struct cpu_time cpu;
	struct memory_report memory;
};

int infomgr_rcv(char *source, int (*r_cb)(struct infomgr_report *),
		int (*raw_cb)(struct infomgr_raw_report *));

#endif
