#[allow(unused)]
use {
    jlogger_tracing::{
        jdebug, jerror, jinfo, jtrace, jwarn, JloggerBuilder, LevelFilter, LogTimeFormat,
    },
    std::os::fd::AsRawFd,
    std::{
        env,
        fs::{self, canonicalize, create_dir_all, remove_file, File},
        os::unix::fs::symlink,
        path::{Path, PathBuf},
        process::{self, Command, Stdio},
        thread::available_parallelism,
    },
};

fn main() {
    JloggerBuilder::new()
        .max_level(LevelFilter::DEBUG)
        .log_file(Some(("/tmp/infomgr.log", false)))
        .log_console(false)
        .build();

    let out_dir = env::var("OUT_DIR").unwrap();
    jinfo!("{}", out_dir);

    let top_dir = env::current_dir()
        .unwrap()
        .as_path()
        .to_str()
        .unwrap()
        .to_owned();
    let schem_dir = format!("{}/schem/", top_dir);

    env::set_current_dir(Path::new(&out_dir))
        .unwrap_or_else(|e| panic!("Failed to set current to {}: {}", out_dir, e));

    Command::new("flatc")
        .args(["--rust", &format!("{}/infomgr.fbs", schem_dir)])
        .spawn()
        .unwrap_or_else(|e| panic!("Failed to generate flat buffer code: {}.", e));

    println!("cargo:rerun-if-changed={}", schem_dir);
}
